sudo apt-get install -y git
if [ -n "$(which pip)" ]; then
	echo "Already Installed"
else
	wget https://bootstrap.pypa.io/get-pip.py
	python get-pip.py
fi
sudo pip install -r requirements.txt

# create schema
python manage.py syncdb
# populate data
python moviebooking/setup.py
# runserver at port 8000
python manage.py runserver
