import threading, json

from django.http import HttpResponse, HttpResponseRedirect, HttpResponseBadRequest,HttpResponseNotFound
from django.views.decorators.csrf import csrf_exempt
from random import randint
from time import sleep
from moviebooking.models import *

BLOCKING_TIMEOUT_IN_SECS = 20

def generate_booking_id():
	new_booking_id=randint(0,65536)
	while MovieSeats.objects.filter(booking_id=new_booking_id).count()!=0:
		new_booking_id=randint(0,65536)
	return new_booking_id

def set_seat_booking_timeout(seats_to_unlock, timeout_in_secs):
	print "set_seat_booking_timeout starting timeout : %s secs" %(timeout_in_secs)
	sleep(timeout_in_secs)
	if MovieSeats.objects.filter(id__in=seats_to_unlock, seat_status= MovieSeats.SEAT_STATUSES['LOCKED']).count() == len(seats_to_unlock):
		MovieSeats.objects.filter(id__in=seats_to_unlock).update(
				seat_status=MovieSeats.SEAT_STATUSES['AVAILABLE'],
				booking_id=None,
				booking_name=None
			)
	print "set_seat_booking_timeout complete"

@csrf_exempt
def movie_seat_list(request):
	if request.REQUEST.has_key('movie_id') and request.REQUEST['movie_id'] is not None:
		movie_seats = MovieSeats.objects.filter(movie_id=request.REQUEST['movie_id'])
		return HttpResponse(json.dumps([
					{
						'seat_name':"R%sC%s" %(seat.seat_row_num, seat.seat_col_num),
						'seat_id':seat.id,
						'seat_status': seat.seat_status
					} for seat in movie_seats
				]),mimetype="application/json",status=200)

@csrf_exempt
def book(request):
	if request.REQUEST.has_key('seats_to_lock') and request.REQUEST.has_key('movie_id')\
		and request.REQUEST['seats_to_lock'] is not None and request.REQUEST['movie_id'] is not None:
		
		seats_to_lock = list(json.loads(request.REQUEST['seats_to_lock']))
		if len(seats_to_lock) >0 and\
		 len(seats_to_lock) == MovieSeats.objects.filter(id__in=seats_to_lock, seat_status= MovieSeats.SEAT_STATUSES['AVAILABLE']).count():
			booking_id = generate_booking_id()
			booking_name = 'new_user'
			MovieSeats.objects.filter(id__in=seats_to_lock, seat_status= MovieSeats.SEAT_STATUSES['AVAILABLE']).update(
				seat_status=MovieSeats.SEAT_STATUSES['LOCKED'],
				booking_id=booking_id,
				booking_name=booking_name
				)
			threading.Thread(target=set_seat_booking_timeout,
				kwargs={'seats_to_unlock':seats_to_lock, 'timeout_in_secs':BLOCKING_TIMEOUT_IN_SECS}).start()
			return HttpResponse(booking_id,mimetype="application/json",status=200)
		return HttpResponse("unavailable",mimetype="application/json",status=401)

	## on any failure
	return HttpResponse("null",mimetype="application/json",status=401)

@csrf_exempt
def confirm(request):
	if request.REQUEST.has_key('booking_id') and request.REQUEST['booking_id'] is not None:
		my_movie_seats = MovieSeats.objects.filter(booking_id=request.REQUEST['booking_id'])
		my_movie_seats_count = my_movie_seats.count()
		if my_movie_seats_count > 0 and my_movie_seats.filter(seat_status=MovieSeats.SEAT_STATUSES['LOCKED']).count() == my_movie_seats_count:
			my_movie_seats.update(
				seat_status=MovieSeats.SEAT_STATUSES['CONFIRMED'])
			return HttpResponse(json.dumps({
				'booking_id' : request.REQUEST['booking_id'],
				'booked_seats' : ["R%sC%s" %(seat.seat_row_num, seat.seat_col_num) for seat in my_movie_seats]
				}),mimetype="application/json",status=200)

		## on any failure
		return HttpResponse("timedout",mimetype="application/json",status=200)
	else:
		## on any failurefrom moviebooking.models import *
		return HttpResponse("null",mimetype="application/json",status=200)

