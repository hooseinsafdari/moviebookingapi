from django.db import models

class Theatre(models.Model):
	DEFAULT_ROWS = 10
	DEFAULT_COLS = 10

	name = models.CharField(max_length=200,blank=True, null=True)
	total_rows = models.IntegerField(blank=True,null=True, default=DEFAULT_ROWS)
	total_cols = models.IntegerField(blank=True,null=True, default= DEFAULT_COLS)

class MovieDetails(models.Model):
	movie_name = models.CharField(max_length=200,blank=True, null=True)
	movie_price = models.IntegerField(blank=True,null=True, default=100)
	time_start = models.DateTimeField(null = True, blank = True)
	time_end = models.DateTimeField(null = True, blank = True)
	theatre = models.ForeignKey(Theatre,blank=True,null=True)

class MovieSeats(models.Model):
	SEAT_STATUSES = {
		'AVAILABLE' : 0,
		'LOCKED' : 1,
		'CONFIRMED': 2
	}
	movie = models.ForeignKey(MovieDetails,blank=False,null=False)
	seat_row_num = models.IntegerField(blank=False,null=False)
	seat_col_num = models.IntegerField(blank=False,null=False)
	seat_status = models.IntegerField(blank=False,null=False, default=SEAT_STATUSES['AVAILABLE'])
	booking_id = models.IntegerField(blank=True,null=True)
	booking_name = models.CharField(max_length=200,blank=True, null=True)
