from django.contrib import admin
from moviebooking.models import Theatre, MovieDetails, MovieSeats
# Register your models here.
admin.site.register(Theatre)
admin.site.register(MovieDetails)
admin.site.register(MovieSeats)
