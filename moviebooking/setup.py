import sys
import os
import datetime
sys.path.append('./')
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "MovieBookingAPI.settings")

from moviebooking.models import *

def populate_movie_theatre_with_seats(theatre_name, movie_name, time_start, time_end):
	theatre, created = Theatre.objects.get_or_create(name=theatre_name)
	# TODO:: CONDITION TO CHECK IF TWO MOVIES NOT CLASHING
	# if MovieDetails.objects.filter(time_end__gte=)
	movie, created = MovieDetails.objects.get_or_create(
			movie_name=movie_name,
			theatre=theatre,
			time_start=time_start,
			time_end=time_end
		)

	for row_num in range(0,theatre.total_rows):
		for col_num in range(0,theatre.total_cols):
			MovieSeats.objects.get_or_create(
				seat_row_num=row_num,
				seat_col_num=col_num,
				movie=movie
			)


if __name__ == '__main__':
	populate_movie_theatre_with_seats(theatre_name = 'Theatre1',
		movie_name = 'MyMovie',
		time_start = datetime.datetime(2016,6,3),
		time_end  = datetime.datetime(2016,6,3)
		)
