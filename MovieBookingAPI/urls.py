from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.conf import settings

admin.autodiscover()

urlpatterns = patterns('',
	url(r'^admin/', include(admin.site.urls)),
	url(r'^book/', 'moviebooking.views.book', name='book'),
	url(r'^confirm/', 'moviebooking.views.confirm', name='confirm'),
	url(r'^movie_seat_list/', 'moviebooking.views.movie_seat_list', name='movie_seat_list'),
	url(r'^booking/(?P<path>.*)$', 'django.views.static.serve',
		{'document_root': settings.STATIC_ROOT, 'show_indexes': False}),
	)